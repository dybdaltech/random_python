import random
import time

alphabet = ["a", "b", "c", "d", "!", " ", ",",  "e", "f", "g", "q", "w", "r", "t", "y", "u", "i", "o", "p", "å", "\^", "s", "h", "j", "k", "l", "ø", "æ", "z", "x", "v", "n", "m", ",", ".", ";", ":", "-", "_", "A", "B", "C", "D", "E", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

startTime = time.time()
isCorrect = False
correctWord = "Hello World"
correctArr = []
result = ''
i = 0
previousArr = []
attempt = 0
attempts = []
rgen = 0
wordAttempt = []

while not isCorrect:
    prevRgen = rgen
    rgen = random.randrange(0, len(alphabet))
    selectedChar = alphabet[rgen]
    if selectedChar in wordAttempt:
        while selectedChar in wordAttempt:    
            rgen = random.randrange(0, len(alphabet))
            selectedChar = alphabet[rgen]
    if selectedChar in correctWord:
        if selectedChar in list(correctWord[i]):
            i = i+1
            attempts.append(attempt)
            correctArr.append(selectedChar)
            attempt = 0
    else:
        wordAttempt.append(selectedChar)
        attempt = attempt+1
    if result.join(correctArr) == correctWord:
        isCorrect = True
        
    print(result.join(correctArr))
        

#Finished with the loop, print results
endTime = time.time()
j = 0
total = 0

print(" ")
print("Attempts statistics for: " + str(result.join(correctArr)))
print("---------------------------------------------")
#Print the number of guesses
for letter in correctArr:
    print(letter + " | " + str(attempts[j]))
    j = j+1
for number in attempts:
    total = total + number
elapsedTime = endTime-startTime
print("Total attempts: " + str(total))
print("Total time elapsed in seconds: " + str(elapsedTime))
print(" ")
print(wordAttempt)
#with open('afterOpti.txt', 'a') as f:
#    f.write(str(total)+ "\n")
#    f.write(str(elapsedTime) + "\n")
#    f.write("------------------" + "\n")

#Advanced algorithm
#Check if the letter has already been guessed, if yes then don't guess it. Partially finished
#
#
